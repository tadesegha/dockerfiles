augroup myautocmds
  autocmd!

  " always open help files in a vertical split
  autocmd FileType help wincmd L

  " do not display side numbers for terminal
  " autocmd TermOpen * setlocal norelativenumber

  " close the quick fix window when i leave the window
  autocmd BufLeave * if getbufvar(winbufnr(winnr()), "&buftype") == "quickfix"|q|endif
  autocmd FileType qf 20wincmd_

  " always go into insert mode in a terminal
  " autocmd BufEnter * call Epic_global_insertIfTerminal()

  " setup for a cs file
  autocmd FileType cs packadd omnisharp-vim
  autocmd FileType cs packadd syntastic
  autocmd FileType cs packadd ultisnips
  autocmd FileType cs execute "source " . g:nvimfiles . "/" . "mappings/cs.vim"
  autocmd FileType cs execute "source " . g:nvimfiles . "/" . "functions/cs.vim"
  autocmd FileType cs execute "source " . g:nvimfiles . "/" . "settings/cs.vim"
  autocmd FileType cs execute "source " . g:nvimfiles . "/" . "commands/cs.vim"
  autocmd BufEnter *.cs OmniSharpAddToProject | SyntasticCheck

  " setup for a js file
  autocmd FileType javascript execute "source " . g:nvimfiles . "/" . "mappings/js.vim"
  autocmd FileType javascript packadd ultisnips

  " setup for a sql file
  autocmd FileType sql execute "source " . g:nvimfiles . "/" . "settings/sql.vim"
augroup END

let g:nvimfiles = expand("<sfile>:h") . '/'

execute "source " . g:nvimfiles . "packages.vim"
execute "source " . g:nvimfiles . "settings/settings.vim"
execute "source " . g:nvimfiles . "mappings/mappings.vim"
execute "source " . g:nvimfiles . "functions/functions.vim"
execute "source " . g:nvimfiles . "autocmds.vim"

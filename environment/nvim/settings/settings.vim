" ===============================================================================
" vim-settings
" ===============================================================================
set autoindent
set completeopt=menu,longest
set encoding=utf-8 " ensures listchars display properly especially when in terminal
set expandtab
set hidden
set ignorecase
set inccommand=nosplit
set list
set listchars=eol:¬,trail:·,tab:»\
set nobackup
set nocursorline
set nonumber
set noswapfile
set path=.,,**
set relativenumber
set shiftwidth=2
set smartcase
set smartindent
set softtabstop=2
set splitright
set tabstop=2
set foldmethod=indent
set foldlevelstart=99
set nowrap
set runtimepath+=~/.common/public/nvim/runtime
set laststatus=2

" ===============================================================================
" choose random colorscheme from available list
" ===============================================================================
let colorschemes = ['spacegray']
let colorSchemeIndex = (reltimestr(reltime())[-1:] % len(colorschemes))
let colorscheme = colorschemes[colorSchemeIndex]

execute 'colorscheme ' . colorscheme

" ===============================================================================
" neosnippet settings
" ===============================================================================
let g:neosnippet#disable_runtime_snippets = { '_' : 1 }

" ===============================================================================
" fzf settings
" ===============================================================================
let $FZF_DEFAULT_COMMAND = 'ag --ignore .git --ignore bin --ignore obj --ignore node_modules --ignore packages --ignore build -g .'

" ===============================================================================
" omnisharp settings
" ===============================================================================
let g:OmniSharp_server_path = 'c:\tools\omnisharp-roslyn\artifacts\scripts\omnisharp.http.cmd'
let g:OmniSharp_use_random_port = 1
let g:OmniSharp_want_snippet = 1
let g:OmniSharp_timeout = 5

" ===============================================================================
" ultisnips settings
" ===============================================================================
let g:UltiSnipsJumpForwardTrigger = '<tab>'

" ===============================================================================
" syntastic settings
" ===============================================================================
let g:syntastic_cs_checkers = ['code_checker']

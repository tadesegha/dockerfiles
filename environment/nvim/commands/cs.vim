" commands
command! -buffer -nargs=1 -complete=file NewItem :call Epic_cs_newItem(<f-args>)
command! -buffer -nargs=1 ExtractVariable :call Epic_cs_extractVariable(<f-args>)
command! -buffer -nargs=1 FindInSolution :call EpicFindInSolution(<f-args>)
command! -buffer -nargs=1 AddField :call Epic_cs_addField(<f-args>)
command! -buffer -nargs=+ AddUsing :call Epic_cs_addUsing(<f-args>)
command! -buffer -nargs=1 -complete=dir MoveItem :call Epic_cs_moveItem(<f-args>)
command! -buffer -nargs=1 Port :call Epic_cs_setOmniSharpPort(<args>)

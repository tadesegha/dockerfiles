" ===============================================================================
" minpac (plugin manager)
" ===============================================================================
packadd minpac

call minpac#init()
call minpac#add('k-takata/minpac', { 'type': 'opt' })
call minpac#add('junegunn/fzf')
call minpac#add('junegunn/fzf.vim')
call minpac#add('moll/vim-bbye')
call minpac#add('Shougo/neosnippet.vim', { 'type': 'opt' })
call minpac#add('tadesegha/omnisharp-vim', { 'type': 'opt' })
call minpac#add('vim-syntastic/syntastic', { 'type': 'opt' })
call minpac#add('sirver/ultisnips', { 'type': 'opt' })

if (!exists('g:epic_cs_functions_loaded') || !g:epic_cs_functions_loaded)
  let g:epic_cs_functions_loaded = 1
  let g:quickfixHeight = 20

  function! FindImplementations()
    OmniSharpFindImplementations
    call s:ExpandQuickFixWindow()
  endfunction

  function! FindMembers()
    OmniSharpFindMembers
    call s:ExpandQuickFixWindow()
  endfunction

  function! FindUsages()
    OmniSharpFindUsages
    call s:ExpandQuickFixWindow()
  endfunction

  function! s:ExpandQuickFixWindow()
    if (len(filter(getwininfo(), 'v:val.quickfix')) > 0)
      execute 'copen ' . g:quickfixHeight
    endif
  endfunction

  function! RunAllTests()
    update
    call s:OpenTestOutput()
    execute 'silent read !powershell -command "t"'
  endfunction

  function! RunTestsInFile()
    update
    let namespace = s:GetFullyQualifiedClassName()

    if (match(namespace, 'Test') == -1)
      call GotoAlternateFile()
      let namespace = s:GetFullyQualifiedClassName()
    endif

    call s:OpenTestOutput()
    execute 'silent read !powershell -command "t ' . namespace . '"'
  endfunction

  function! s:OpenTestOutput()
    if (bufexists('testOutput'))
      buffer testOutput
      normal ggdG
    else
      enew
      file testOutput
    endif

    setlocal buftype=nofile
    setlocal bufhidden=hide
  endfunction

  function! s:GetNamespace()
    let namespace = expand("%:.:h:gs?\\?.?")
    let namespace = substitute(namespace, '^src.', '', '')
    return namespace
  endfunction

  function! s:GetFullyQualifiedClassName()
    let namespace = s:GetNamespace()
    let class = expand("%:t:r")
    return namespace . "." . class
  endfunction

  function! GotoAlternateFile()
    if (match(expand('%'), 'Test') != -1)
      call s:GotoSut()
    else
      call s:GotoTest()
    endif
  endfunction

  function! s:GotoTest()
    let filename = expand('%:t:r')
    let testFilename = filename . 'Test'

    execute 'edit **/' . testFilename . '*.cs'
  endfunction

  function! s:GotoSut()
    let filename = expand('%:t:r')
    let sutFilename = substitute(filename, 'Tests\?', "", "")

    execute 'edit **/' . sutFilename . '.cs'
  endfunction

  function! Epic_cs_testTeardown()
    let content = [
          \ '[TearDown]',
          \ 'public void TearDown() => mockRepository.VerifyAll();']

    call append(line('.'), content)
    OmniSharpCodeFormat
  endfunction

  function! Epic_cs_testSetup()
    let content = [
          \ '[SetUp]',
          \ 'public void SetUp()',
          \ '{',
          \ '}',
          \ '']

    let lineNum = line('.')
    call append(lineNum, content)
    OmniSharpCodeFormat
    call cursor(lineNum + 3, 100)
  endfunction

  function! Epic_cs_testNew()
    let content = [
          \ '[Test]',
          \ 'public void Should()',
          \ '{',
          \ '}']

    let lineNum = line('.')

    let isEmptyLine = Epic_global_isEmptyLine()
    if (!isEmptyLine)
      call append(lineNum, [''])
      let lineNum = lineNum + 1
    endif

    call append(lineNum, content)
    OmniSharpCodeFormat
    call cursor(lineNum + 2, 27)

    startinsert
  endfunction

  function! Epic_cs_addToProject()
    let csproj = s:FindCsproj()
    let csprojContents = readfile(csproj)

    if (match(csprojContents[0], '<Project Sdk=') != -1)
      echom 'no need to modify csproj of a .net standard project'
      return
    endif

    let directory = fnamemodify(csproj, ':h')
    let pattern = escape(directory . '\', '\')
    let path = substitute(expand('%'), pattern, "", "")

    let pattern = '<Compile Include="' . escape(path, '\') . '" />'
    if (match(csprojContents, pattern) != -1)
      echom 'file is already in csproj'
      return
    endif

    tabnew
    execute 'edit ' . csproj
    normal gg
    execute '/<Compile Include='
    execute 'normal o<Compile Include="' . path . '" />'
    update
    bd

    echom "File added to csproj"
  endfunction

  function! Epic_cs_removeFromCsproj()
    let csproj = s:FindCsproj()
    let csprojContents = readfile(csproj)

    if (match(csprojContents[0], '<Project Sdk=') != -1)
      echom 'no need to modify csproj of a .net standard project'
      return
    endif

    let directory = fnamemodify(csproj, ':h')
    let pattern = escape(directory . '\', '\')
    let path = substitute(expand('%'), pattern, "", "")

    let pattern = '<Compile Include="' . escape(path, '\') . '" />'
    if (match(readfile(csproj), pattern) == -1)
      echom 'File not in csproj'
      return
    endif

    tabnew
    execute 'edit ' . csproj
    normal gg
    execute '/<Compile Include="' . escape(path, '\') . '" />'
    normal dd
    update
    bd

    echom "File removed from csproj"
  endfunction

  function! Epic_cs_setOmniSharpPort(port)
    let g:OmniSharp_port = a:port
    let g:OmniSharp_host = 'http://localhost:' . a:port
  endfunction

  function! EpicFindInSolution(pattern)
    lgetexpr system('ag --csharp ' . a:pattern)
    lopen
  endfunction

  function! Epic_cs_extractVariable(content)
    let name = Epic_global_getInput('Variable name')
    let lineNum = line('.')

    execute ":%s/\\V" . a:content . "/" . name . "/g"
    call s:CursorPosition(lineNum, 0)

    let declaration = "var " . name . " = " . a:content . ";"
    call append(line('.') - 1, declaration)

    :OmniSharpCodeFormat
  endfunction

  function! Epic_cs_newItem(path)
    call s:CreateFile(a:path)

    if (match(a:path, '\ctest') == -1)
      call s:ClassTemplate()
    else
      call s:TestTemplate()
    endif
  endfunction

  function! Epic_cs_deleteItem(path)
    execute "edit! " . a:path
    call s:EmptyFile()
    call Epic_cs_removeFromCsproj()

    Bd!
    call delete(a:path)
  endfunction

  function! Epic_cs_moveItem(path)
    let oldpath = expand("%")
    let newpath = substitute(a:path, '/', '\\', 'g') . ".cs"

    let csproj = s:FindCsproj(newpath)
    if (IsEmpty(csproj))
      echom "no csproj found for " . newpath
      return
    endif

    update

    let directory = fnamemodify(newpath, ':h')
    call system('mkdir ' . directory)
    call system('copy ' . oldpath . ' ' . newpath)
    call Epic_cs_deleteItem(oldpath)

    execute "edit " . newpath
    call Epic_cs_addToProject()
    call s:UpdateNamespace()
    call s:UpdateClassName()

    echom "successfully moved item"
  endfunction

  function! Epic_cs_addField(declaration)
    let position = getpos('.')
    let line = search('class', 'bn')

    let noScope = (match(a:declaration, 'public') == -1) && (match(a:declaration, 'private') == -1)
    let newline = match(a:declaration, ' $') != -1

    let declaration = noScope ? 'private readonly ' . a:declaration . ';' : a:declaration . ';'

    call s:AddLine(declaration, line + 1)
    if (newline)
      call s:AddLine('', line + 2)
    endif

    OmniSharpCodeFormat

    call setpos('.', position)
    normal j
  endfunction

  function! Epic_cs_addUsing(namespace)
    let linenum = search('^using.*;$', 'bn', '1')
    let statement = 'using ' . a:namespace . ';'

    call append(linenum, statement)
  endfunction

  function! s:FindCsproj(...)
    let path = (a:0 > 0) ? a:1 : expand('%')
    let directory = fnamemodify(path, ':h')
    let csproj = globpath(directory, '*.csproj')

    while csproj == "" && strlen(directory) > 0
      let directory = strpart(directory, 0, strridx(directory, '\'))
      let csproj = globpath(directory, '*.csproj')
    endwhile

    return csproj
  endfunction

  function! s:CreateFile(input)
    if (match(a:input, '.cs *$') == -1)
      let path = a:input . '.cs'
    else
      let path = a:input
    endif

    let directory = fnamemodify(path, ':h')
    call system('mkdir ' . directory)
    execute "edit " . path

    call Epic_cs_addToProject()
  endfunction

  function! s:ClassTemplate()
    let filename = s:Filename()

    if (match(filename, "^I") == -1)
      let type = 'class'
    else
      let type = 'interface'
    endif

    let lines = [ 'namespace ' . s:GetNamespace(), '{', 'public ' . type . ' ' . filename, '{', '}', '}' ]
    call setline(1, lines)

    :OmniSharpCodeFormat
    update

    call s:CursorPosition(4, 5)
  endfunction

  function! s:TestTemplate()
    let lines = [
          \'using NUnit.Framework;',
          \'',
          \'namespace ' . s:GetNamespace(),
          \'{',
          \'[TestFixture]',
          \'public class ' . s:Filename(),
          \'{',
          \'}',
          \'}'
          \]
    call setline(1, lines)

    :OmniSharpCodeFormat
    update

    call s:CursorPosition(7, 9)
  endfunction

  function! s:CursorPosition(lineNum, colNum)
    call setpos('.', [0, a:lineNum, a:colNum, 0])
  endfunction

  function! s:UpdateNamespace()
    let lineNum = search('namespace')
    if (lineNum == 0)
      echom 'no namespace declaration found'
      return
    endif

    let declaration = getline(lineNum)
    let newDeclaration = substitute(declaration, 'namespace \zs.*', s:GetNamespace(), '')

    if (declaration == newDeclaration)
      echom 'no change in namespace'
      return
    endif

    call setline(lineNum, newDeclaration)
  endfunction

  function! s:Filename()
    return expand("%:t:r")
  endfunction

  function! s:EmptyFile()
    normal ggdG
  endfunction

  function! s:UpdateClassName()
    update
    let class = searchpos('class \zs.*')
    call setpos('.', class)
    execute ':OmniSharpRenameTo ' . s:Class()
  endfunction

  function! s:Class()
    return expand("%:t:r")
  endfunction

  function! s:FullBodiedMember()
    let position = getpos('.')

    let lineNum = line('.')
    let line = getline(lineNum)

    let returnValue = (match(line, 'void') == -1) ? 'return' : ''

    let pattern = '> *$'
    if (match(line, pattern) == -1)
      let body = substitute(line, '[A-z].*=>', returnValue, '')
      delete
    else
      let body = returnValue . ' ' . getline(lineNum + 1)
      .,+1 delete
    endif

    let lineNum = line('.') - 1
    let declaration = substitute(line, ' =>.*', '', '')
    call append(lineNum, [declaration, '{', body, '}'])
    OmniSharpCodeFormat

    call setpos('.', position)
  endfunction

  function! s:ExpressionBodiedMember()
    let position = getpos('.')

    let declaration = getline('.') . ' =>'
    let oldBody = getline(line('.') + 2)
    let body = substitute(oldBody, 'return ', '', '')

    call setline(line('.'), [declaration, body])

    +2,+3 delete
    call setpos('.', position)
  endfunction

  function! s:AddLine(content, ...)
    let lineNum = (a:0 > 0) ? a:1 : line('.')
    let lineHasContent = match(getline(lineNum), '^ *$') == -1

    if (lineHasContent)
      call append(lineNum, a:content)
      call s:CursorPosition(lineNum + 1, 0)
    else
      call setline(lineNum, a:content)
    endif
  endfunction
endif
